import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      // appBar: AppBar(
      //   title: const Text("Profile"),
      //   centerTitle: true,
      //   backgroundColor: Colors.transparent,
      //   elevation: 0,
      // ),
      extendBody: true,
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          ClipPath(
            child: Container(
              height: 150,
              decoration: const BoxDecoration(
                color: Color.fromARGB(255, 7, 22, 159),
              ),
            ),
          ),
          ClipPath(
            child: Container(
              height: 220,
              decoration: const BoxDecoration(
                color: Color.fromARGB(255, 7, 22, 159),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20),
                ),
              ),
              child: const Center(
                child: ListTile(
                  leading: Icon(Icons.menu, size: 40, color: Colors.white),
                    title: Text(
                      "Hello",
                      style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w100, color: Colors.white),
                    ),
                    subtitle: Text(
                      "Assi Christian Joel",
                      style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                  )
              )
              
            ),
          ),
          ListView(
            padding: const EdgeInsets.all(8.0),
            children: [
              const SizedBox(height: 140.0),
              Card(
                color: Colors.white,
                elevation: 0,
                margin: const EdgeInsets.symmetric(
                  vertical: 8.0,
                  horizontal: 32.0,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text( 
                            "Naira Wallet :",
                            style: Theme.of(context).textTheme.subtitle1,
                          ),
                          Text(
                            "3,000,000",
                            style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400, color : Colors.black)
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text( 
                            "N 25,329.15",
                            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color : Color.fromARGB(255, 7, 22, 159) ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(5), 
                            child: FlatButton(
                              padding: EdgeInsets.all(2),
                              color: Colors.grey.withOpacity(0.5),
                              splashColor: Colors.black12,
                              onPressed: (){
                              },
                              child: Text(
                                "FUND WALLET",
                                style: TextStyle(fontSize: 10),
                              ),
                            )
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text( 
                            "Lorem Ispum Lorem Ispum ",
                            style: Theme.of(context).textTheme.subtitle1,
                          ),
                          TextButton(onPressed: (){}, child: Text(
                                "Refresh",
                                style: TextStyle(fontSize: 15, color: Colors.black),
                              ),)
                        ],
                      ),
                      
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 20.0),
              const Padding( 
                padding: EdgeInsets.only(left: 20),
                child : Text(
                "Services",
                style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Color.fromARGB(255, 7, 22, 159)),
              )),
              const SizedBox(height: 10.0),
              SizedBox(
                height: 150,
                width: MediaQuery.of(context).size.width,
                child: Wrap(
                  // direction: Axis.vertical,
                  // alignment: WrapAlignment.center,
                  // spacing:8.0,
                  // runAlignment:WrapAlignment.center,
                  // runSpacing: 8.0,
                  // crossAxisAlignment: WrapCrossAlignment.center,
                  // textDirection: TextDirection.rtl,
                  // verticalDirection: VerticalDirection.up,
                  children: <Widget>[
                    
                    _buildCard("hello"),
                    _buildCard("hello"),
                    _buildCard("hello"),
                    _buildCard("hello"),
                    _buildCard("hello"),
                    _buildCard("hello"),
                    _buildCard("hello"),
                    _buildCard("hello"),
                    _buildCard("hello"),
                    _buildCard("hello"),
                    _buildCard("hello"),
                    _buildCard("hello"),

                  ]
                )
              ),
            ],
          )
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            label: 'Account',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.school),
            label: 'Transactions',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_balance),
            label: 'profile',
          ),
        ],
        currentIndex: 0,
        showUnselectedLabels : true,
        selectedLabelStyle: TextStyle(color: Color.fromARGB(255, 7, 22, 159)),
        unselectedLabelStyle: TextStyle(color: Color.fromARGB(255, 7, 22, 159)),
        unselectedItemColor: Color.fromARGB(255, 7, 22, 159).withOpacity(0.5),
        selectedItemColor: Color.fromARGB(255, 7, 22, 159),
        onTap: (index){},
      ),
    );
  }

  Widget _buildCard(String title, {Widget? icon, Color? color }) {
    
    return Container(
      margin: const EdgeInsets.only(left:5, right: 5, bottom: 10, top: 10),
      decoration: BoxDecoration(
        color: color != null ? color : Colors.blue.withOpacity(0.4),
        borderRadius: BorderRadius.circular(15)
      ),
      width: MediaQuery.of(context).size.width / 4.7,
      height: 120,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          icon != null ? icon : Icon(Icons.wifi, size: 40,),
          Text(  title , textScaleFactor: 1.5, )
        ],
      )
    );
  }
}
